#pragma once

#ifdef VOLUMECONTROLUTILS_API
#define VOLUMECONTROLUTILS_API __declspec(dllexport)
#else
#define VOLUMECONTROLUTILS_API __declspec(dllimport)
#endif

/// <summary>
/// Set process volume value
/// </summary>
/// <param name="pid">Process ID to set volume</param>
/// <param name="percentage">New percentage volume (from 0 to 100)</param>
/// <returns></returns>
extern "C" VOLUMECONTROLUTILS_API bool SetPIDVolume(int pid, int percentage);

/// <summary>
/// Set master volume value
/// </summary>
/// <param name="percentage">New percentage volume (from 0 to 100)</param>
/// <returns></returns>
extern "C" VOLUMECONTROLUTILS_API bool SetMasterVolume(int percentage);

/// <summary>
/// Mute master channel volume (all applications)
/// </summary>
/// <param name="muteStatus">Boolean value mute state</param>
/// <returns></returns>
extern "C" VOLUMECONTROLUTILS_API bool MuteMasterVolume(bool muteStatus);