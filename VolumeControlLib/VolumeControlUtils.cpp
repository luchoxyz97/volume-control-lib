#include "pch.h"
#include <stdio.h>
#include <Windows.h>
#include <Mmdeviceapi.h>
#include <audiopolicy.h>
#include <endpointvolume.h>
#include "VolumeControlUtils.h"

extern const UUID MMDeviceEnumUUID = __uuidof(MMDeviceEnumerator);
extern const UUID IMMDeviceUUID = __uuidof(IMMDeviceEnumerator);

extern const UUID IAudioSessionManager2UUID = __uuidof(IAudioSessionManager2);
extern const UUID IAudioSessionControl2UUID = __uuidof(IAudioSessionControl2);

extern const UUID IAudioEndpointVolumeUUID = __uuidof(IAudioEndpointVolume);

extern const UUID ISimpleAudioVolumeUUID = __uuidof(ISimpleAudioVolume);

bool notifyIfFailed(HRESULT result)
{
	if (FAILED(result))
	{
		printf("Ha ocurrido un error, por ende, no se ha podido cambiar el volumen.");
		return true;
	}

	return false;
}

IMMDevice* GetDefaultAudioDevice()
{
	HRESULT result = NULL;
	IMMDeviceEnumerator* deviceEnumerator = NULL;
	IMMDevice* device = NULL;

	//Get all sound streaming devices
	result = CoCreateInstance(MMDeviceEnumUUID, NULL, CLSCTX_INPROC_SERVER, IMMDeviceUUID, (LPVOID*)&deviceEnumerator);
	if (notifyIfFailed(result)) return NULL;

	//Get default sound stream device
	result = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &device);
	deviceEnumerator->Release();
	if (notifyIfFailed(result)) return NULL;

	return device;
}

bool SetMasterVolume(int percentage) {
	HRESULT result = NULL;
	IMMDevice* device = NULL;
	IAudioEndpointVolume *audioEndpointVolume = NULL;

	//Initialize the co-creation
	result = CoInitialize(NULL);

	device = GetDefaultAudioDevice();

	result = device->Activate(IAudioEndpointVolumeUUID, CLSCTX_INPROC_SERVER, NULL, (LPVOID*)&audioEndpointVolume);
	device->Release();

	float volume = percentage * 0.01;
	result = audioEndpointVolume->SetMasterVolumeLevelScalar(volume, NULL);
	audioEndpointVolume->Release();

	if (notifyIfFailed(result)) return false;
	CoUninitialize();
	return true;
}

bool MuteMasterVolume(bool muteStatus) {
	HRESULT result = NULL;
	IMMDevice* device = NULL;
	IAudioEndpointVolume* audioEndpointVolume = NULL;

	//Initialize the co-creation
	result = CoInitialize(NULL);

	device = GetDefaultAudioDevice();

	result = device->Activate(IAudioEndpointVolumeUUID, CLSCTX_INPROC_SERVER, NULL, (LPVOID*)&audioEndpointVolume);
	device->Release();

	result = audioEndpointVolume->SetMute(muteStatus, NULL);
	audioEndpointVolume->Release();

	if (notifyIfFailed(result)) return false;
	CoUninitialize();
	return true;
}

bool SetPIDVolume(int pid, int percentage)
{
	HRESULT result = NULL;
	IMMDevice* device = NULL;

	IAudioSessionManager2* audioSessionManager2 = NULL;
	IAudioSessionEnumerator* audioSessionEnumerator = NULL;

	//Initialize the co-creation
	result = CoInitialize(NULL);

	device = GetDefaultAudioDevice();

	result = device->Activate(IAudioSessionManager2UUID, CLSCTX_INPROC_SERVER, NULL, (LPVOID*)&audioSessionManager2);
	device->Release();
	if (notifyIfFailed(result)) return false;

	result = audioSessionManager2->GetSessionEnumerator(&audioSessionEnumerator);
	audioSessionManager2->Release();
	if (notifyIfFailed(result)) return false;

	int sessionCount = 0;
	result = audioSessionEnumerator->GetCount(&sessionCount);
	if (notifyIfFailed(result)) return false;

	for (int i = 0; i < sessionCount; i++)
	{
		IAudioSessionControl* audioSessionControl = NULL;
		result = audioSessionEnumerator->GetSession(i, &audioSessionControl);
		if (notifyIfFailed(result)) return false;

		IAudioSessionControl2* audioSessionControl2 = NULL;
		result = audioSessionControl->QueryInterface(IAudioSessionControl2UUID, (LPVOID*)&audioSessionControl2);
		if (notifyIfFailed(result)) return false;

		ISimpleAudioVolume* sessionVolumeManager = NULL;
		result = audioSessionControl->QueryInterface(ISimpleAudioVolumeUUID, (LPVOID*)&sessionVolumeManager);
		if (notifyIfFailed(result)) return false;

		audioSessionControl->Release();

		DWORD processId = NULL;
		result = audioSessionControl2->GetProcessId(&processId);
		if (notifyIfFailed(result)) return false;

		audioSessionControl2->Release();

		float volume = percentage * 0.01;
		if (processId == pid)
		{
			result = sessionVolumeManager->SetMasterVolume(volume, NULL);
			sessionVolumeManager->Release();
		}

	}
	audioSessionEnumerator->Release();
	//Uninitialize the co-creation after ends all operations
	CoUninitialize();

	return true;
}